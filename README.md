## INTRODUCTION

The Scheduler Content Moderation Sidebar module integrates the Scheduler Content Moderation Integration module with the Moderation Sidebar module.

It comes with a form mode that should be leveraged to display the scheduler fields in the sidebar.

## REQUIREMENTS

Depends on:

- [Scheduler Moderation Integration](https://www.drupal.org/project/scheduler_moderation_sidebar)
- [Moderation Sidebar](https://www.drupal.org/project/moderation_sidebar)

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION

- Go to the `Manage form display` page of the content type you want to use the scheduler with.
- Enable the `Moderation` form mode and edit it.
- Disable all fields except the `Scheduler` fields: Scheduler Dates, Publish on, Publish state, Unpublish on and Unpublish state.
