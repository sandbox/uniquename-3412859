<?php

declare(strict_types = 1);

namespace Drupal\scheduler_moderation_sidebar\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for Scheduler Content Moderation Sidebar routes.
 */
final class SchedulerFormController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function __invoke(Request $request, EntityInterface $entity): array {

    $form = $this->entityFormBuilder()->getForm($entity, 'moderation');

    unset($form['actions']['preview']);
    unset($form['actions']['delete']);
    unset($form['advanced']);

    $build['back_button'] = $this->getBackButton($entity);
    $build['content'] = $form;

    return $build;
  }

  /**
   * Generates the render array for an AJAX-enabled back button.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   An entity.
   *
   * @return array
   *   A render array representing a back button.
   */
  protected function getBackButton(ContentEntityInterface $entity) {
    $params = [
      'entity' => $entity->id(),
      'entity_type' => $entity->getEntityTypeId(),
    ];

    $back_url = Url::fromRoute('moderation_sidebar.sidebar', $params);

    $build = [
      '#type' => 'container',
      '#attributes' => ['class' => ['moderation-sidebar-container']],
      [
        '#title' => $this->t('← Back'),
        '#type' => 'link',
        '#url' => $back_url,
        '#attributes' => [
          'class' => ['use-ajax', 'moderation-sidebar-back-button'],
          'data-dialog-type' => 'dialog',
          'data-dialog-renderer' => 'off_canvas',
        ],
      ],
    ];

    return $build;
  }

}
